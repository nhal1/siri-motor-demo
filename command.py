import RPi.GPIO as GPIO
import time
import os


GPIO.setmode(GPIO.BCM)
ACT_FILE = "/home/pi/.action"
FORWARD = 6
BACKWARDS = 13
LEFT  = 19
RIGHT = 26
ALL = (FORWARD, BACKWARDS, LEFT, RIGHT)
GPIO.setup(ALL, GPIO.OUT)
GPIO.output(ALL, 0)


actions = {
        'forward': FORWARD,
        'back': BACKWARDS,
        'left': LEFT,
        'right': RIGHT}

while(1):
    if os.path.exists(ACT_FILE):
        try:
            with open(ACT_FILE, "r") as af:
                user_data = af.read().replace('\n', '').split()
            print(user_data)
            action = user_data[0]
            duration = float(user_data[1])
            GPIO.output(actions[action], 1)
            time.sleep(duration)
            GPIO.output(actions[action], 0)
        except Exception as e:
            print(e)
        finally:
            os.remove(ACT_FILE)
    else:
        time.sleep(0.5)


